#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 15 13:51:15 2023

@author: mlamoure
"""

import autograd as ag
import autograd.numpy as np


from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt

def f (x,y):
    return x**3*y - 3*x**2*(y - 1) + y**2 - 1

def g (x,y):
    return x**2*y**2 - 2


# Calculés avec Maple
abscisses = [-1.896238074, -0.4638513741, 1.568995999]
ordonnees = [0.6844390650, -0.8460057970, -0.4658228710]

x = -1.5
y = -.97

fig = plt.figure(figsize = (20,10))
ax = fig.add_subplot(1, 1, 1, projection='3d')

ax.set_xlabel('$a$', labelpad=20)
ax.set_ylabel('$b$', labelpad=20)
ax.set_zlabel('$f(a,b)$', labelpad=20)

xplot = np.arange (-3.5, 1.7, 0.1)
yplot = np.arange (-2, 0.8, 0.1)

X, Y = np.meshgrid (xplot, yplot)
Z = f(X,Y)

ax.plot_surface(X, Y, Z, cmap="spring_r", lw=0.5, rstride=1, cstride=1, alpha=0.5)
ax.contour(X, Y, Z, 10, colors="k", linestyles="dashed")
ax.contour(X, Y, Z, 0, colors="blue",  levels=np.array([0], dtype=np.float64), linestyles="solid")

for i in range (len(abscisses)) :
    ax.scatter (abscisses[i], ordonnees[i], f(abscisses[i], ordonnees[i]), color='black')

#########################################################
# 2ème sous-graphe

ax = fig.add_subplot(1, 2, 2, projection='3d')

ax.set_xlabel('$a$', labelpad=20)
ax.set_ylabel('$b$', labelpad=20)
ax.set_zlabel('$g(a,b)$', labelpad=20)

xplot = np.arange (-3.5, 1.7, 0.1)
yplot = np.arange (-2, 0.8, 0.1)

X, Y = np.meshgrid (xplot, yplot)
Z = g(X,Y)

ax.plot_surface(X, Y, Z, cmap="autumn_r", lw=0.5, rstride=1, cstride=1, alpha=0.5)
ax.contour(X, Y, Z, 10, colors="k", linestyles="dashed")
ax.contour(X, Y, Z, 0, colors="green",  levels=np.array([0], dtype=np.float64), linestyles="solid")

for i in range (len(abscisses)) :
    ax.scatter (abscisses[i], ordonnees[i], g(abscisses[i], ordonnees[i]), color='black')

# plt.savefig ("../deux-variables-graphe.png")

plt.show()

#Question 1 
#On observe graphiquement que x=-0.78 et y=1.88
#On observe graphiquement que x=-2.9 et y=0.52



#Question 2
def Jac (x,y) :
    return np.array ([[3*x**2*y - 6*x*(y - 1), x**3 - 3*x**2 + 2*y], [2*x*y**2, 2*x**2*y]], dtype=np.float64)

#question 3
def F(u) :
    a = u[0]
    b = u[1]
    return np.array ([a**3*b - 3*a**2*(b - 1) + b**2 - 1, a**2*b**2 - 2])

def J_F(u) :
    a = u[0]
    b = u[1]
    return np.array ([[3*a**2*b - 6*a*(b - 1), a**3 - 3*a**2 + 2*b], [2*a*b**2, 2*a**2*b]], dtype=np.float64)

u = np.array([-0.8,1.9], dtype=np.float64)

for n in range (0,7) :
    h = np.linalg.solve (- J_F(u), F(u))
    u = u + h
print(u)

u = np.array([-2.9,0.5], dtype=np.float64)

for n in range (0,7) :
    h = np.linalg.solve (- J_F(u), F(u))
    u = u + h
print(u)

#Question 4

import autograd.numpy as np
from autograd import jacobian


def F(u):
    a, b = u[0], u[1]
    return np.array([a**3*b - 3*a**2*(b - 1) + b**2 - 1, a**2*b**2 - 2])

# Utilisation de la fonction autograd.jacobian pour calculer la jacobienne
J_F = jacobian(F)

u = np.array([-0.8, 1.9], dtype=np.float64)

for n in range(0, 7):
    h = np.linalg.solve(-J_F(u), F(u))
    u = u + h

print(u)

def F(u):
    a, b = u[0], u[1]
    return np.array([a**3*b - 3*a**2*(b - 1) + b**2 - 1, a**2*b**2 - 2])

# Utilisation de la fonction autograd.jacobian pour calculer la jacobienne
J_F = jacobian(F)

u = np.array([-2.9,0.5], dtype=np.float64)

for n in range(0, 7):
    h = np.linalg.solve(-J_F(u), F(u))
    u = u + h

print(u)

#Question 5 
# Utilisation du mode forward pour calculer la jacobienne

def F(u):
    a = u[0]
    b = u[1]
#Pour f(a,b)
    t1 = a ** 2
    t2 = b ** 2
    t3 = (a*b - 3*b + 3)*t1 +t2-1
    
#Pour g(a,b)
    t1 = a**2
    t2 = b**2
    t4 = t1*t2-2
    return np.array([t3,t4], dtype=np.float64)

def Jac_F_forward(u):
    a = u[0]
    b = u[1]
#Pour f(a,b)
    t1 = a ** 2
    t2 = b ** 2
    t3 = (a*b - 3*b + 3)*t1 +t2-1
    dt1_da = 2*a
    dt1_db = 0
    dt2_da = 0
    dt2_db = 2*b
    dt3_da = b*t1 + (a*b-3*b+3)*dt1_da + dt2_da
    dt3_db = (a-3)*t1 + (a*b-3*b+3)*dt1_db + dt2_db
    
#Pour g(a,b)
    t1 = a**2
    t2 = b**2
    t4 = t1*t2-2
    dt1_da = 2*a
    dt1_db = 0
    dt2_da = 0
    dt2_db = 2*b
    dt4_da = dt1_da*t2+t1*dt2_da
    dt4_db = dt1_db*t2+t1*dt2_db
    return np.array([[dt3_da, dt3_db],[dt4_da,dt4_db]], dtype=np.float64)

print('Jacobienne avec autograd')
j = ag.jacobian(F)
print(j(u))
print('Jacobienne avec la fonction écrite')
print(J_F(u))
print('Jacobienne avec la méthode forward')
print(Jac_F_forward(u))

print ("On obtient bien les mêmes résultats avec les différentes méthodes !")