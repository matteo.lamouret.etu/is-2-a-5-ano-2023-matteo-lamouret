#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  1 14:21:49 2023

@author: mlamoure
"""

# Question 4
import autograd as ag
import autograd.numpy as np
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt

def f (a,b):
    return a**3 + 2*a**2 - 2*a*b + b**2 + a*b**3 - 2*b + 5

def nabla_f (a,b) :
    return np.array ([b**3 + 3*a**2 + 4*a - 2*b, 
                      3*a*b**2 - 2*a + 2*b - 2], dtype=np.float64)

fig = plt.figure(figsize = (20,20))
ax = plt.axes(projection='3d')

ax.set_xlabel('$a$', labelpad=20)
ax.set_ylabel('$b$', labelpad=20)
ax.set_zlabel('$f(a,b)$', labelpad=20)

aplot = np.arange (-.8, 1.2, 0.05)
bplot = np.arange (-.8, 1.2, 0.05)

# Tracé du graphe de f
A, B = np.meshgrid (aplot, bplot)
Z = f(A,B)
Z = np.clip (Z, 3, 12)

ax.plot_surface(A, B, Z, cmap="spring_r", lw=0.5, rstride=1, cstride=1, alpha=0.5)
ax.contour(A, B, Z, 30, colors="k", linestyles="dotted")

# Le minimim local de f sans tenir compte de la contrainte
zero = np.array ([0.2255014396, 0.9318083312])
ax.scatter ([zero[0]], [zero[1]], [f(zero[0],zero[1])], color='black')

# Tracé de la contrainte a**2 + b**2 = r**2 (avec r**2 = 1/2)

def c (a,b) :
    return a**2 + b**2 - 1/2

def nabla_c (a,b) :
    return np.array ([2*a,2*b], dtype=np.float64)

# Pour éviter que le tracé de la contrainte perturbe le tracé du graphe de f
zmin = np.amin (Z)
zmax = np.amax (Z)
ax.set_zlim3d (zmin,  zmax)

# On définit le graphe de la contrainte comme une courbe de niveau
# dans le plan horizontal
Z = c(A,B)
C = ax.contour(A, B, Z, [0])
# On supprime l'objet graphique produit par la courbe dans le plan horizontal
for objet in ax.collections :
    objet.remove ()
# ax.collections.pop ()
# On trace la courbe 3D des points du graphe de f qui satisfont la contrainte
for ii, seg in enumerate(C.allsegs[0]) :
    z = f (seg[:,0],seg[:,1])
    z = np.clip (z, zmin, zmax)
    ax.plot (seg[:,0], seg[:,1], z, color='green')

# Tracé d'un gradient (longueur normalisée)
for a in [.2, .3, .4] :
    b = np.sqrt(1/2 - a**2)
    grad_f = nabla_f (a, b)
    grad_f = (.25/np.linalg.norm(grad_f,2)) * grad_f
    ax.quiver (a, b, f(a,b), grad_f[0], grad_f[1], 0, color='blue')
    ax.text (a, b, f(a,b), 'un gradient de f')
    


    grad_c = nabla_c (a, b)
    grad_c = (.3/np.linalg.norm(grad_c,2)) * grad_c
    ax.quiver (a, b, f(a,b), grad_c[0], grad_c[1], 0, color='red')
    ax.text (a, b, f(a,b), 'un gradient de c')
plt.show ()
print(" Question 4 \n")
print("Le minimul local (2) n apparait pas sur la courbe. En effet 0.2255**2 + 0.9312**2 > 0.5 /n " )
print(" Le point optimum est (0.2, 0.7). \n ")
print(" 0.2**2 + 0.7**2 = 0.5 Donc la contrainte sera active et la solution optimale est sur la frontière \n")
print("Le maximul local est (0.5, -0.5) \n")
print(" la contrainte d'égalité est a**2 + b**2 = 1/2 \n")

# Question 5

print("Question 5 \n")
print("Les points (a,b) qui satisfont la contraite et où le gradient de la contrainte et celui de l'objectif sont parallèles : \n")
print("minimum : (a,b) = (0.2, 0.8) \n")
print("maximum : (a,b) = (0.5, -0.5) \n")


print("Question 6 \n")
print("En affichant des vecteurs, nous obtenons bien les mêmes résultats \n")


print("Question 7 \n")

def L(u):
    a, b, lmbda = u
    return f(a,b) + lmbda*c(a,b)

nabla_L = ag.grad(L)
H_L = ag.hessian(L)

for u0 in [np.array([.2,.8], dtype=np.float64), np.array([0.5,-0.5], dtype=np.float64)]:
    u = np.array([u0[0], u0[1], 0], dtype = np.float64)
    print('point de départ =', u)
    for n in range (5):
        g = nabla_L(u)
        H = H_L(u)
        h = np.linalg.solve(H, -g)
        u = u + h
        print('u =', u)
    
print("La solution optimale est alors (a,b) = (0.1853, 0.6824) ")

print("Question 11 \n")

def objectif(v):
    x1, x2, x3, x4, x5 = v[0], v[1], v[2], v[3], v[4]
    y1, y2, y3, y4, y5 = v[5], v[6], v[7], v[8], v[9]
    return (4000*x1 + 5000*x2)**2 - ((8*y1 + 7*y2 + 3*y3)**2)

print("question 12 \n")
def contraintes(v):
    x1, x2, x3, x4, x5 = v[0], v[1], v[2], v[3], v[4]
    y1, y2, y3, y4, y5 = v[5], v[6], v[7], v[8], v[9]
    c1 = 2*x1 + x2 + x3 - 8
    c2 = x1 + 2*x2 + x4 - 7
    c3 = x2 + x5 - 3
    c4 = x1 * y4
    c5 = x2 * y5
    c6 = 2*y1 + y2 - y4 - 4000
    c7 = y1 + 2*y2 + y3 - y5 - 5000
    c8 = x3*y1
    c9 = x4 * y2
    c10 = x5 * y3
    return np.array([c1, c2, c3, c4, c5, c6, c7, c8, c9, c10])
    
print("Question 13 \n")  
print("Pour écrire le Lagrangien du problème, nous devons introduire un multiplicateur",
      "de Lagrange pour chaque contrainte du problème. Dans notre cas, nous avons dix contraintes (trois contraintes du primal, deux contraintes du dual et cinq produits de variables issus du théorème des écarts complémentaires). Par conséquent, nous avons besoin de dix multiplicateurs de Lagrange.")

print("Le nombre total de variables, y compris les variables originales et les multiplicateurs de Lagrange, est donc de 10+10=20 variables")

def lagrangien(v, u):
    x1, x2, x3, x4, x5 = v[0], v[1], v[2], v[3], v[4]
    y1, y2, y3, y4, y5 = v[5], v[6], v[7], v[8], v[9]
    lambda1, lambda2, lambda3, lambda4, lambda5, lambda6, lambda7, lambda8, lambda9, lambda10 = u[0], u[1], u[2], u[3], u[4], u[5], u[6], u[7], u[8], u[9]

    f_val = (4000*x1 + 5000*x2)**2 - ((8*y1 + 7*y2 + 3*y3)**2)

    c1 = 2*x1 + x2 + x3 - 8
    c2 = x1 + 2*x2 + x4 - 7
    c3 = x2 + x5 - 3
    c4 = x1 * y4
    c5 = x2 * y5
    c6 = 2*y1 + y2 - y4 - 4000
    c7 = y1 + 2*y2 + y3 - y5 - 5000
    c8 = x3 * y1
    c9 = x4 * y2
    c10 = x5 * y3

    lagrangian_val = f_val + lambda1 * c1 + lambda2 * c2 + lambda3 * c3 + lambda4 * c4 + lambda5 * c5 + lambda6 * c6 + lambda7 * c7 + lambda8 * c8 + lambda9 * c9 + lambda10 * c10

    return lagrangian_val
      
  
print("Question 14 \n")    


      
      
      
      
      
      
      
