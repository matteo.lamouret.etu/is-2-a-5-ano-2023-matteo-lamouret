#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 24 15:15:13 2023

@author: mlamoure
"""

#!/bin/python3
#Question 1

import autograd as ag
import autograd.numpy as np
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
from autograd import grad, hessian


def f (a,b):
    return a**3 + 2*a**2 - 2*a*b + b**2 + a*b**3 - 2*b + 5


fig = plt.figure(figsize = (20,20))
ax = plt.axes(projection='3d')

ax.set_xlabel('$a$', labelpad=20)
ax.set_ylabel('$b$', labelpad=20)
ax.set_zlabel('$f(a,b)$', labelpad=20)

xplot = np.arange (-.5, 2, 0.05)
yplot = np.arange (-.5, 3, 0.05)

X, Y = np.meshgrid (xplot, yplot)
Z = f(X,Y)
Z = np.clip (Z, 3, 8)

ax.plot_surface(X, Y, Z, cmap="spring_r", lw=0.5, rstride=1, cstride=1, alpha=0.5)
ax.contour(X, Y, Z, 30, colors="k", linestyles="dotted")

plt.show ()


print("On appelle point stationnaire d’un objectif tout point x* appartenant à Rm qui annule sa matrice jacobienne ou son gradient.")

print("Dans cette fonction, on observe graphiquement trois points stationnaires : (0.6, -2.1), (-1.3, -0.2) et (-0.4, 1,4)\n")


#Question 2

import numpy as np
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt


def f (a,b):
    return a**3 + 2*a**2 - 2*a*b + b**2 + a*b**3 - 2*b + 5

def nabla_f (a,b) :
    return np.array ([3*a**2 + 4*a - 2*b + b**3, -2*a + 2*b + 3*b**2*a - 2], dtype=np.float64)

def H_f (a,b) :
    return np.array ([[6*a+4, -2 + 6*b**2], [-2 + 3*b**2, 2 + 6*b*a]], dtype=np.float64)

print("les 3 valeurs exactes des points stationnaires sont donc : \n")

fig = plt.figure(figsize = (20,20))
ax = plt.axes(projection='3d')

ax.set_xlabel('$a$', labelpad=20)
ax.set_ylabel('$b$', labelpad=20)
ax.set_zlabel('$f(a,b)$', labelpad=20)

# xplot = np.arange (-.3, .5, 0.05)
# yplot = np.arange ( .8, 1.6, 0.05)
xplot = np.arange (-.5, 2, 0.05)
yplot = np.arange (-.5, 3, 0.05)

X, Y = np.meshgrid (xplot, yplot)
Z = f(X,Y)
Z = np.clip (Z, 3, 8)

ax.plot_surface(X, Y, Z, cmap="spring_r", lw=0.5, rstride=1, cstride=1, alpha=0.5)
ax.contour(X, Y, Z, 30, colors="k", linestyles="dotted")

ax.scatter (1/3, 4/3, f(1/3,4/3), color='black')
zero = np.array ([1/3, 4/3], dtype=np.float64)

a0 = 1
b0 = -1/0.9
u = np.array ([a0,b0], dtype=np.float64)
for i in range (4) :
    a, b = u
    z = f(a,b)
    ax.scatter (a, b, z, color='blue')
    etiq = '$\\mathbf{u_' + str(i) + '}$'
    ax.text (a+.02, b, z+.05, etiq)
    H_f_u = H_f (a,b)
    nabla_f_u = nabla_f (a,b)
    h = np.linalg.solve (- H_f_u, nabla_f_u)
    u = u + h
print("u1:", u)

plt.show ()

fig = plt.figure(figsize = (20,20))
ax = plt.axes(projection='3d')

ax.set_xlabel('$a$', labelpad=20)
ax.set_ylabel('$b$', labelpad=20)
ax.set_zlabel('$f(a,b)$', labelpad=20)

# xplot = np.arange (-.3, .5, 0.05)
# yplot = np.arange ( .8, 1.6, 0.05)
xplot = np.arange (-.5, 2, 0.05)
yplot = np.arange (-.5, 3, 0.05)

X, Y = np.meshgrid (xplot, yplot)
Z = f(X,Y)
Z = np.clip (Z, 3, 8)

ax.plot_surface(X, Y, Z, cmap="spring_r", lw=0.5, rstride=1, cstride=1, alpha=0.5)
ax.contour(X, Y, Z, 30, colors="k", linestyles="dotted")

ax.scatter (1/3, 4/3, f(1/3,4/3), color='black')
zero = np.array ([1/3, 4/3], dtype=np.float64)

a0 = -1/0.8
b0 = -1/5
u = np.array ([a0,b0], dtype=np.float64)
for i in range (4) :
    a, b = u
    z = f(a,b)
    ax.scatter (a, b, z, color='blue')
    etiq = '$\\mathbf{u_' + str(i) + '}$'
    ax.text (a+.02, b, z+.05, etiq)
    H_f_u = H_f (a,b)
    nabla_f_u = nabla_f (a,b)
    h = np.linalg.solve (- H_f_u, nabla_f_u)
    u = u + h
print("u2 :", u)

plt.show ()


fig = plt.figure(figsize = (20,20))
ax = plt.axes(projection='3d')

ax.set_xlabel('$a$', labelpad=20)
ax.set_ylabel('$b$', labelpad=20)
ax.set_zlabel('$f(a,b)$', labelpad=20)

# xplot = np.arange (-.3, .5, 0.05)
# yplot = np.arange ( .8, 1.6, 0.05)
xplot = np.arange (-.5, 2, 0.05)
yplot = np.arange (-.5, 3, 0.05)

X, Y = np.meshgrid (xplot, yplot)
Z = f(X,Y)
Z = np.clip (Z, 3, 8)

ax.plot_surface(X, Y, Z, cmap="spring_r", lw=0.5, rstride=1, cstride=1, alpha=0.5)
ax.contour(X, Y, Z, 30, colors="k", linestyles="dotted")

ax.scatter (1/3, 4/3, f(1/3,4/3), color='black')
zero = np.array ([1/3, 4/3], dtype=np.float64)

a0 = -1/1.6
b0 = 1/0.6
u = np.array ([a0,b0], dtype=np.float64)
for i in range (4) :
    a, b = u
    z = f(a,b)
    ax.scatter (a, b, z, color='blue')
    etiq = '$\\mathbf{u_' + str(i) + '}$'
    ax.text (a+.02, b, z+.05, etiq)
    H_f_u = H_f (a,b)
    nabla_f_u = nabla_f (a,b)
    h = np.linalg.solve (- H_f_u, nabla_f_u)
    u = u + h
print("u3 :", u )

plt.show ()


#Question 4

print (" \n D’après la proposition 18, un point stationnaire x˚ est un minimum local si et seulement si toutes les valeurs propres de la matrice hessienne Hf(x*) sont strictement positives.")



points_stationnaires = [(0.62682974, -1.95041445), (-1.23852572, -0.17900019), (-0.2113376, 1.58644881)]

for i, point in enumerate(points_stationnaires):
    print(f"Point u{i + 1} : {point}")
    H_point = H_f(point[0], point[1])
    eigenvalues = np.linalg.eigvals(H_point)
    print(f"Valeurs propres de la hessienne : {eigenvalues}")
    
print(" \n Il n'y a aucun point pour lesquels toutes les valeurs propres de la matrice Hessienne sont définies positives donc il n'y a pas de minima locaux ")
    
#Question 5

def f(a, b):
    return a**3 + 2*a**2 - 2*a*b + b**2 + a*b**3 - 2*b + 5

# Définir la fonction fbis pour autograd
def fbis(u):
    a = u[0]
    b = u[1]
    return f(a, b)

# Utiliser autograd pour calculer le gradient de fbis
nabla_f_autograd = grad(fbis)

# Utiliser autograd pour calculer la hessienne de fbis
H_f_autograd = hessian(fbis)

# Question 1 : la visualisation reste la même

# Question 2 : nous n'avons plus besoin de ces fonctions

# Question 3 : 
    
points_stationnaires = [(0.62682974, -1.95041445), (-1.23852572, -0.17900019), (-0.2113376, 1.58644881)]

# Afficher les coordonnées des points stationnaires
for i, point in enumerate(points_stationnaires):
    gradient_value = nabla_f_autograd(np.array(point))
    hessian_value = H_f_autograd(np.array(point))
    print(f"Point u{i + 1} : {point}")
    print(f"Gradient : {gradient_value}")
    print(f"Hessienne : {hessian_value}\n")
    

# Question 4 : 
    

print(" \n u1 : (0.62682974, -1.95041445) \n Valeurs propres de la hessienne : [7.76097844, -5.3354667] \n Les deux valeurs propres ne sont pas strictement positives, donc ce point n'est pas un minimum local")

print(" \n u2 : (-1.23852572, -0.17900019) \n Valeurs propres de la hessienne : [-3.43115432, 3.33017804] \n Les deux valeurs propres ne sont pas strictement positives, donc ce point n'est pas un minimum local")

print(" \n u3 : (-0.2113376, 1.58644881) \n Valeurs propres de la hessienne : [2.7319744, -0.0116577] \n Les deux valeurs propres ne sont pas strictement positives, donc ce point n'est pas un minimum local")

print(" \n En résumé, aucun des points stationnaires fournis ne correspond à un minimum local de la fonction f, car aucune des matrices hessiennes évaluées en ces points n'a toutes ses valeurs propres strictement positives.")

#Question 6



def nabla_fter(u):
    a = u[0]
    b = u[1]
    t1 = b ** 2
    t2 = a ** 2
    t3 = a * t1 * b - 2 * a * b + t2 * a - 2 * b + t1 + 2 * t2 + 5
    
    df_dt3 = 1
    df_dt2 = 2 * a * df_dt3
    df_dt1 = t1 * b * df_dt3 - 2 * b * df_dt2 + t2 * df_dt3
    df_da = b * df_dt1
    df_db = a * df_dt1 + 2 * a * t1 * b * df_dt3 - 2 * df_dt2
    
    return np.array([df_da, df_db], dtype=np.float64)

def H_fter(u):
    a = u[0]
    b = u[1]
    t1 = b ** 2
    t2 = a ** 2
    t3 = a * t1 * b - 2 * a * b + t2 * a - 2 * b + t1 + 2 * t2 + 5
    
    df_dt3 = 1
    df_dt2 = 2 * a * df_dt3
    df_dt1 = t1 * b * df_dt3 - 2 * b * df_dt2 + t2 * df_dt3
    
    d2f_dt1a = b * df_dt3
    d2f_dt1b = t1 * df_dt3
    d2f_dt2a = 2 * df_dt3
    d2f_dt2b = 2 * a * df_dt3
    d2f_dt3a = b * df_dt1
    d2f_dt3b = t1 * df_dt1 - 2 * df_dt2
    
    d2f_da = np.array([d2f_dt1a, d2f_dt2a, d2f_dt3a], dtype=np.float64)
    d2f_db = np.array([d2f_dt1b, d2f_dt2b, d2f_dt3b], dtype=np.float64)
    
    H = np.array([d2f_da, d2f_db], dtype=np.float64)
    return H.T  # Transposer la matrice hessienne pour obtenir la forme correcte

u1 = np.array([0.62682974, -1.95041445])
u2 = np.array([-1.23852572, -0.17900019])
u3 = np.array([-0.2113376, 1.58644881])

# Calculer le gradient et la hessienne pour chaque point
gradient_u1 = nabla_fter(u1)
hessian_u1 = H_fter(u1)

gradient_u2 = nabla_fter(u2)
hessian_u2 = H_fter(u2)

gradient_u3 = nabla_fter(u3)
hessian_u3 = H_fter(u3)

print("Gradient au point u1 :", gradient_u1)
print("Hessienne au point u1 :\n", hessian_u1)

print("Gradient au point u2 :", gradient_u2)
print("Hessienne au point u2 :\n", hessian_u2)

print("Gradient au point u3 :", gradient_u3)
print("Hessienne au point u3 :\n", hessian_u3)

print(" \n Pour u1, u2 et u3, ils possèdent tous au moins une valeur propre negative donc il n'y a pas de minimum locaux")
print (" \n Pour conclure, avec les trois méthodes différentes, on observe qu'il n'y a pas de minimum locaux !")