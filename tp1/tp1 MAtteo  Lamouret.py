#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 13 09:13:59 2023

@author: mlamoure
"""
import autograd as ag
import autograd.numpy as np
import numpy as np
import scipy.linalg as nla
import matplotlib.pyplot as plt

#1 - Moindres carrés linéaires

# Fonction de la cubique
def f(x, alpha, beta, gamma, mu):
    return alpha*x**3 + beta*x**2 + gamma*x + mu

# Paramètres initiaux
alpha, beta, gamma, mu = 0.5, -2, 1, 7

# Points expérimentaux
Tx = np.array([-1.1, 0.17, 1.22, -0.5, 2.02, 1.81])
p = Tx.shape[0]
Ty_sur_la_courbe = np.array([f(x, alpha, beta, gamma, mu) for x in Tx])
perturbations = 0.5 * np.array([-1.3, 2.7, -5, 0, 1.4, 6])
Ty_experimentaux = Ty_sur_la_courbe + perturbations

# Erreur initiale
erreur_initiale = nla.norm(Ty_sur_la_courbe - Ty_experimentaux, 2)

# Tracer la courbe initiale et les points expérimentaux
xplot = np.linspace(-1.2, 2.1, 100)
yplot_initiale = [f(x, alpha, beta, gamma, mu) for x in xplot]

plt.scatter(Tx, Ty_experimentaux, color='blue', label='Points expérimentaux')
plt.plot(xplot, yplot_initiale, color='blue', label='Courbe initiale')
plt.title('Courbe initiale et points expérimentaux')
plt.legend()
plt.show()

# Construction du système d'équations linéaires Ax = b
A = np.empty([p, 4], dtype=np.float64)
b = Ty_experimentaux

for i in range(p):
    A[i, 0] = Tx[i]**3
    A[i, 1] = Tx[i]**2
    A[i, 2] = Tx[i]
    A[i, 3] = 1

# Résolution du système linéaire
ATA = np.dot(np.transpose(A), A)
ATb = np.dot(np.transpose(A), b)

# Solution optimale
x_optimal = nla.solve(ATA, ATb)

# Mise à jour des paramètres
alpha, beta, gamma, mu = x_optimal

# Calcul de l'erreur minimale avec les nouveaux paramètres
Ty_optimal = np.array([f(x, alpha, beta, gamma, mu) for x in Tx])
erreur_minimale = nla.norm(Ty_experimentaux - Ty_optimal, 2)

# Vérification de l'erreur minimale inférieure à l'erreur initiale
if erreur_minimale < erreur_initiale:
    print("L'erreur minimale est inférieure à l'erreur initiale.")

# Tracer la courbe optimale en orange
yplot_optimal = [f(x, alpha, beta, gamma, mu) for x in xplot]

plt.scatter(Tx, Ty_experimentaux, color='blue', label='Points expérimentaux')
plt.plot(xplot, yplot_initiale, color='blue', label='Courbe initiale')
plt.plot(xplot, yplot_optimal, color='orange', label='Courbe optimale')
plt.title('Courbe optimale et points expérimentaux')
plt.legend()
plt.show()

#2 - Méthode de Newton en une variable

# question 2
def f(x):
    return x**3 - 2

def fprime(x):
    return 3 * x**2

u=2
for n in range (0, 6):
    print ('u[%d] =' % n, u)
    u = u-f(u)/fprime(u)
    
print ('les valeurs de u convergent rapidement vers la racine cubique de 2 à mesure que les itérations progressent')

#question 3
alpha, beta, gamma, mu = 1.5870106105525719, -3.1447447637103476, -0.37473299354311346, 7.484053576868045

def f(x):
    return alpha*x**3 + beta*x**2 + gamma*x + mu

def fprime(x) :
    return 3*alpha*x**2 + 2*beta*x + gamma

def fseconde (x) :
    return 6*alpha*x + 2*beta
u = 0.8
for n in range (10) :
    print ('u[%d] ='% n, u)
    u = u - fprime(u)/fseconde(u)
    
print ('Les valeurs de u convergent vers le minimum local de la cubique à mesure que les itérations progressent')

#Question 4

import autograd.numpy as np
from autograd import grad, hessian

alpha, beta, gamma, mu = 1.5870106105525719, -3.1447447637103476, -0.37473299354311346, 7.484053576868045

def f(x):
    return alpha*x**3 + beta*x**2 + gamma*x + mu

# Calcul de la dérivée première et seconde
f_prime = grad(f)
f_second = hessian(f)

u = 0.8
for n in range(10):
    print('u[%d] =' % n, u)
    u = u - f_prime(u) / f_second(u)

print('Nous retrouvons bien les mêmes résultats')
