# is4-ano-2023

Ce projet est dédié au cours d'Algorithmique Numérique pour l'Optimisation
en IS2A5.

Chaque apprenti est censé :

1. créer sa propre copie du projet par un fork

2. rendre sa copie privée (Settings / General / Visibility)

3. inviter François Boulier comme membre du projet avec
                            (Manage / Members / Max role et Expiration) :

    - le statut Maintainer
    - une date limite fixée au 15 septembre 2024

4. dans chacune de ses copies de travails, créer un alias pour désigner
    le projet initial (celui de l'enseignant, pas le sien !) :

    - git remote add projet-initial https://gitlab.univ-lille.fr/francois.boulier/is2a5-ano-2023
